from django.db import models
import uuid
from django.contrib.auth.models import AbstractUser
from datetime import datetime


# Create your models here.

class User(AbstractUser):
    """

    """
    location = models.CharField(max_length=30, blank=True)
    phone_number = models.CharField( max_length=13, blank=True)
    bio = models.CharField(blank= True,max_length=100) 
    photo = models.ImageField(blank = True)


class Kurse(models.Model):
    """
    main table for klasses
    just need to finde a way to performe video and text and other stuff

    """

    auther = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length = 20 , help_text = "Name of the klass")
    create_date = models.DateTimeField(auto_now_add = True)
    chang_date = models.DateTimeField(auto_now = True)
    photo = models.ImageField(blank = True)
    
    def __str__(self):
        return '{} by {}'.format(self.name , self.auther)
    class Meta:
        ordering = ["-chang_date"]


class Lektion(models.Model):
    """
    every kurs has many lektionen
    """
    name = models.CharField(blank = True , max_length = 20) 
    auther = models.ForeignKey(User , on_delete = models.CASCADE)
    text = models.TextField()
    media = models.FileField(blank = True)
    kurse = models.ForeignKey(Kurse , on_delete = models.CASCADE)
    mod_date = models.DateTimeField(auto_now_add = True)
    chang_date = models.DateTimeField(auto_now = True)


    def __str__(self):
        return '{} of {}'.format(self.name , self.kurse)
    class Meta:
        ordering = ["chang_date"]

    
class Comment(models.Model):
    """
    table for store and use comments
    nothing to do 
    """

    lektion = models.ForeignKey(Lektion , on_delete = models.CASCADE ) 
    auther = models.ForeignKey(User , on_delete = models.CASCADE)
    text = models.CharField(max_length=200)
    kurse = models.ForeignKey(Kurse , on_delete = models.CASCADE)
    create_date = models.DateTimeField(auto_now_add = True)
    chang_date = models.DateTimeField(auto_now = True)
    show = models.BooleanField(default = False)


    def __str__(self):
        return 'Comment by {} on {}'.format(self.auther,self.kurse)
    
    class Meta:
        ordering = ["chang_date"]



class Followe_Kurse(models.Model):
    """
    make 1 V 1 relation between user and followed kurse
    """

    user = models.ForeignKey(User , on_delete = models.CASCADE)
    kurse = models.ForeignKey(Kurse , on_delete = models.CASCADE)

