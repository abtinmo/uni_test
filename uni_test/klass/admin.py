from django.contrib import admin
from .models import  User , Kurse ,Comment ,Lektion , Followe_Kurse
from django.contrib.auth.admin import UserAdmin
# Register your models here.
admin.site.register(User,UserAdmin)
admin.site.register(Kurse)
admin.site.register(Comment)
admin.site.register(Lektion )
admin.site.register(Followe_Kurse)
