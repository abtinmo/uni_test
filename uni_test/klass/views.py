from django.http import HttpResponseForbidden ,HttpRequest
from django.contrib.auth import authenticate , logout , login as a_login
from .models import User , Comment , Kurse , Lektion,Followe_Kurse
from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from .helper import ajax_required
from django.views.decorators.http import require_http_methods
import re
from datetime import datetime

# Create your views here.



def main(request):
    return render(request ,'main.html')

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            a_login(request,user)
            return redirect('chat')
        return render(request,'login.html',{'error':True,})

    else:
        return render(request, 'login.html')

def logout_view(request):
    logout(request)
    return redirect('/')



def register(request):
    if request.method == 'POST':
        try:
            new_user= User.objects.get(username= request.POST["username"])
        except User.DoesNotExist:
            #check if username and password field correctly

            if ( request.POST['password'] == request.POST['passwordRepeat']):
                new_user = User(username = request.POST['username'],)
                new_user.set_password(request.POST["password"])
                new_user.save()
                return redirect('login')
            else:
                return render(request , 'register.html',{'errorpass':True})
                
        else:
            return render(request , 'register.html',{'erroruser':True})
    else:
        return render(request , 'register.html')


@login_required
def profile(request):
    return render(request , 'profile.html')

@login_required
def change_user(request):
    if request.method == 'POST':
        c_user = get_object_or_404(User  , request.user )
        c_user.username = request.POST['newUser']
        c_user.save()
        return render(request ,'profile.html')

@login_required
def change_pass(request):
    if request.method == 'POST':
        c_user = get_object_or_404(User , request.user)
        c_user.set_password(request.POST['changeInputChild'])
        c_user.save()
        logout(request)
        return redirecrt("login")  
    
@login_required
def change_email(request):
    if request.method == 'POST':
        c_user = get_object_or_404(User , request.user)
        userEmail = re.match(r"^.+@.+\..+$" , request.POST['changeInputChild'])
        if userEmail:
            userEmail = request.POST['changeInputChild']
        else:
            userEmail = ''
        c_user.email = userEmail
        c_user.save()
        return render(request , 'profile.html')

@login_required
def chat_view(request):
    kurses = list(Kurse.objects.filter(auther = request.user))
    FollowedByUser = Followe_Kurse.objects.filter(user = request.user)
    for relation in FollowedByUser:
        kurses.append(relation.kurse)
    kurses.sort(key=lambda x: x.chang_date ,reverse = True )
    return render(request , 'chat.html',{'kurses':kurses})

@login_required
def chat_view_thread(request, pk):
    if request.method == 'GET':
        c_kurse = get_object_or_404(Kurse , id=pk)
        kurses = list(Kurse.objects.filter(auther = request.user))
        FollowedByUser = Followe_Kurse.objects.filter(user = request.user)
        for relation in FollowedByUser:
            kurses.append(relation.kurse)
        kurses.sort(key=lambda x: x.chang_date , reverse = True )
        is_followed = False
        is_admin = False
        if c_kurse.auther == request.user:
            is_admin = True
        if c_kurse in kurses:
            is_followed = True
        return render(request , 'chat_thread.html' , {'c_kurse':c_kurse,'kurses':kurses,'is_admin':is_admin,'is_followed':is_followed})
    else:
        return HttpResponseForbidden()


@login_required
def add_lektion(request , pk):
    if request.method == 'POST':
        c_kurse = get_object_or_404(Kurse , id=pk)
        if c_kurse.auther == request.user:
            new_lektion = Lektion(auther = request.user , text = request.POST['content'] ,kurse = c_kurse  )
            new_lektion.save()
            c_kurse.chang_date = datetime.now()
            c_kurse.save()
            return redirect("chat_thread" , pk)
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseForbidden()

@login_required
def add_comment(request ,):
    if request.method == 'POST':
        if 'lektion_radio'  not in request.POST:
            return redirect('chat')
        pk = request.POST['lektion_radio']
        c_lektion = get_object_or_404(Lektion , id = pk)
        new_comment = Comment(lektion = c_lektion , auther = request.user , text = request.POST['content'] , kurse = c_lektion.kurse )
        new_comment.save()
        return redirect("chat_thread" , c_lektion.kurse.id)
    else:
        return HttpResponseForbidden()  


@login_required
def  show_comment(request , pk):
    c_comment = get_object_or_404(Comment , id = pk)
    kurse = c_comment.kurse
    print("salam")
    print(request.user)
    print(kurse.auther)
    if request.user == kurse.auther or request.user == c_comment.auther:
        c_comment.show = not c_comment.show
        c_comment.save()
        return redirect("chat_thread" , kurse.id)

    else: 
        return HttpResponseForbidden()  



@login_required
def remove_comment(request , pk):
    c_comment = get_object_or_404(Comment , id = pk)
    kurse = c_comment.kurse
    if request.user == kurse.auther or request.user == c_comment.auther:
        c_comment.delete()
        return redirect("chat_thread" , kurse.id)

    else: 
        return HttpResponseForbidden()  


  
@login_required
def add_kurse(request):
    if request.method == 'POST':
        new_kurse = Kurse(auther = request.user , name = request.POST['name'])
        new_kurse.save()
        return redirect("chat_thread" , new_kurse.id)
    else:
        return render(request ,'add_kurse.html')

@login_required
def remove_lektion(request , pk):
    c_lektion = get_object_or_404(Lektion , id = pk)
    kurse = c_lektion.kurse
    if request.user == kurse.auther:
        c_lektion.delete()
        return redirect("chat_thread", kurse.id)
    else:
        return HttpResponseForbidden()


def search(request):
    if request.method == 'POST':
        empty = False
        resault = Kurse.objects.filter(name__icontains= request.POST['search'])
        if not resault:
            empty = True
        return render(request , 'search.html',{'resault':resault , 'empty':empty})
    else:
        return render(request , 'search.html')

@login_required
def make_follow(request , pk):
    c_kurse = get_object_or_404(Kurse  , id = pk)
    relation = Followe_Kurse.objects.get_or_create(user = request.user ,kurse = c_kurse)
    return redirect("chat_thread" , c_kurse.id)
