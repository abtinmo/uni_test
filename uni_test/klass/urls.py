from django.urls import path
from django.http import HttpResponse
from . import views

urlpatterns = [
        path('', views.main , name='main'),
        path('login', views.login , name = 'login'),
        path('logout', views.logout_view , name='logout'),
        path('register' , views.register , name = 'register'),
       # path('ajax/validate_username/', views.validate_username, name='validate_username'),
        path('add_kurse' , views.add_kurse , name = 'add_kurse'),
        path('change_pass' , views.change_pass , name = 'change_pass'),
        path('change user', views.change_user , name = 'change_user'),
        path('change_email' , views.change_email , name = 'change_email') ,
        path('chat' , views.chat_view , name = 'chat') ,
        path('chat/<int:pk>' , views.chat_view_thread , name='chat_thread'),
        path('add_lektion/<int:pk>' , views.add_lektion , name = 'add_lektion'),
        path('remove_lektion/<int:pk>' , views.remove_lektion , name = 'remove_lektion'),
        path('add_comment' , views.add_comment , name = 'add_comment'),
        path('remove_comment/<int:pk>' , views.remove_comment , name= 'remove_comment'),
        path('show_comment/<int:pk>'  , views.show_comment , name  = 'show_comment'),
        path('search' , views.search , name = 'search' ),
        path('make_follow/<int:pk>' , views.make_follow , name = 'make_follow'),
        ]
