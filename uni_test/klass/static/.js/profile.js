/* ---------------------------------profile page source --------------------------------------------- */ 
const bars = document.querySelector('i.bars');
const cross = document.querySelector('i.cross');
const menu = document.querySelector('.rightSide');
const changerDiv = document.querySelector('.changerDiv');
const changerButton = document.querySelectorAll('div.changer Button.change');
for(let i = 0; i < changerButton.length; i++){
  changerButton[i].addEventListener('click', () =>{
    changerButton[i].nextElementSibling.classList.toggle('changerDivOpen');
    changerButton[i].nextElementSibling.classList.toggle('fadeIn');
  });
}

//left menu js
bars.addEventListener('click', () => {
  bars.classList.toggle('barsClose');
  cross.classList.toggle('crossOpen');
  menu.classList.toggle('sideOpen');
  menu.classList.add('slideInRight');
});
cross.addEventListener('click', () => {
  bars.classList.toggle('barsClose');
  cross.classList.toggle('crossOpen');
  menu.classList.toggle('sideOpen');
});
/* ---------------------------------end of profile page source --------------------------------------------- */