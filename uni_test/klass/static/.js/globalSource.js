/*----------------------------------danaRegister JS-------------------------------------------*/
const inputs = document.querySelectorAll('input');
const registerButton = document.querySelector('.registerButton');
const patterns = {
  firstName: /^[a-zA-Z\u0600-\u06FF\s]{3,}$/,
  familyName: /^[a-zA-Z\u0600-\u06FF\s]{3,}$/,
  userEmail: /^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/,
  userPhone: /^(0+)(9+)[\d]{9}$/,
  userName: /^[a-zA-Z\u0600-\u06FF\s\d]{4,12}$/,
  userPassword: /^[a-zA-Z\d]{5,20}$/,
  userPasswordRepeat: /^[a-zA-Z\d]{5,20}$/
};
//validation function
function validate(field, regex){
  if(regex.test(field.value)){
    field.style.border = "2px solid green";
    registerButton.disabled = false;
  }else{
    field.style.border = "2px solid red";
    registerButton.disabled = true;
  }
  change();
}
inputs.forEach((input) => {
  input.addEventListener('keyup', (e) => {
    validate(e.target, patterns[e.target.attributes.name.value]);
  });
});
function change(){
  let userPassword = document.querySelector('.userPassword').value;
  let userPasswordRepeat = document.querySelector('.userPasswordRepeat').value;
  let userPasswordLabel = document.querySelector('.userPasswordLabel');
  let userPasswordRepeatLabel = document.querySelector('.userPasswordRepeatLabel');
  if(userPassword == userPasswordRepeat && userPassword.length > 0){
    userPasswordLabel.style.color = "green";
    userPasswordRepeatLabel.style.color = "green";
    userPasswordRepeatLabel.innerHTML = "پسوردها درست است";
  }else{
    userPasswordRepeatLabel.innerHTML = "پسوردها یکی نیستند";
    userPasswordRepeatLabel.style.color = 'red';
    registerButton.disabled = true;
  }
}
/*----------------------------------End Of danaRegister JS-------------------------------------------*/